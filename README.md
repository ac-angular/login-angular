# LoginAngular
Formularios de registro e inicio de sesión. Cliente de la arquitectura cliente-servidor.

## Conceptos a desarrollar
Cliente, Consumo de API, Login, token, expiration

## Tecnologias
Lenguaje: Typescript

Framework: Angular (versión: 8.3.17)

## Uso
Levantar el servidor de desarrollo al ejecutar el siguiente comando:

```bash
1) ng serve -o
```

## Back-End
[Proyecto en GitLab](https://gitlab.com/g-node.js/login-nodejs)

## Referencias
[Domini Code](https://www.youtube.com/watch?v=BCygvtZwkh4)
